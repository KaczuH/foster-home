'use strict';
var gulp = require('gulp'),
    del = require('del'),
    gutil = require('gulp-util'),
// image optimization
    imagemin = require('gulp-imagemin'),
// JS
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
// Sass
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer-core');

var path = require('path');

var staticRoot = 'foster_home/static',
    distPath = staticRoot + '/dist';

var jsDistros = {
    'foster_home': [
        file('js/foster_home.core.js')
    ],
    'vendor-bootstrap': [
        vendorFile('bootstrap-sass-official/assets/javascripts/bootstrap.min.js')
    ],
    'vendor-jquery': [
        vendorFile('jquery/dist/jquery.min.js')
    ]
};

var fontsDirs = [
    vendorFile('bootstrap-sass-official/assets/fonts/**/*')
];

function file(name) {
    return path.join(__dirname, staticRoot, name);
}

function vendorFile(name) {
    return path.join(__dirname, staticRoot, 'vendor', name);
}

var tasks = {
    clean:function(cb){
        del([distPath + '/'], cb);
    },
    jsCompileTask:function(name, filelist) {
        return function() {
            return gulp.src(filelist)
                .pipe(concat(name + '.js'))
                .pipe(gulp.dest(distPath + '/js'))
                .pipe(uglify())
                .pipe(rename(name + '.min.js'))
                .pipe(gulp.dest(distPath + '/js'));
        };
    },
    jsWatchTask:function buildJsWatchTask(name, fileList) {
        return function(){
            return gulp.watch(fileList, ["dist:js:" + name]);
        };
    },
    css:function () { // TODO (knisiki) add css distros concat
        gulp.src(staticRoot + '/sass/**/*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(postcss([autoprefixer({browsers: ['last 2 versions']})]))
            .pipe(gulp.dest(distPath + '/css'));
    },
    fonts: function() {
        return gulp.src(fontsDirs)
            .pipe(gulp.dest(distPath + '/fonts'));
    }
};

function jsCompileDistros(){
    var compileTask, watchTask, fileList, jsDistroNames = [];
    for (var distroName in jsDistros) {
        fileList = jsDistros[distroName];

        compileTask = tasks.jsCompileTask(distroName, fileList);
        watchTask = tasks.jsWatchTask(distroName, fileList);
        gulp.task('dist:js:' + distroName, compileTask);
        gulp.task('watch:js:' + distroName, watchTask);

        jsDistroNames.push(distroName);
    }

    gulp.task('dist:js', jsDistroNames.map(function(n){ return 'dist:js:' + n;}));

    gulp.task("watch:js", jsDistroNames.map(function(n) { return "watch:js:" + n; }));
}

gulp.task('watch:css', function() {
    gulp.watch(staticRoot + '/sass/**/*.scss', tasks.css);
});

gulp.task('watch:fonts', function() {
    gulp.watch(fontsDirs, tasks.fonts);
});

gulp.task('clean', tasks.clean);
jsCompileDistros();
gulp.task('dist:css', tasks.css);
gulp.task('dist:fonts', tasks.fonts);
gulp.task('dist', ['dist:js', 'dist:css', 'dist:fonts']);
gulp.task("watch", ["watch:js", 'watch:css', 'watch:fonts']);
// TODO(kniski) imgmin, watch etc tasks