NPM_ROOT = ./node_modules
STATIC_DIR = foster_home/static

local: setup-git
	@echo "--> Installing dependencies"
	pip install -r ./requirements/local.txt
	npm install

setup-git:
	@echo "--> Installing git hooks"
	cd .git/hooks && ln -sf ../../hooks/* ./
	@echo ""

lint-python:
	@echo "--> Linting Python files"
	PYFLAKES_NODOCTEST=1 flake8 foster_home
	@echo ""

clean: @echo "--> Cleaning static cache"
	${NPM_ROOT}/.bin/gulp clean
	@echo "--> Cleaning .pyc files"
	find . -name "*.pyc" -delete
	@echo ""

.PHONY: local setup-git clean lint-python