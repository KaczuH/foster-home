# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('change_ownership', '0002_auto_20150625_1619'),
    ]

    operations = [
        migrations.AddField(
            model_name='ownershiprequest',
            name='status',
            field=models.CharField(max_length=8, default='PEN', choices=[('ACC', 'Accepted'), ('REJ', 'Rejected'), ('PEN', 'Pending')]),
        ),
    ]
