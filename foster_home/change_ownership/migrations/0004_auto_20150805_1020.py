# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('change_ownership', '0003_ownershiprequest_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='ownershiprequest',
            name='applicant_confirmation',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='ownershiprequest',
            name='owner_confirmation',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='ownershiprequest',
            name='status',
            field=models.CharField(choices=[('ACC', 'Accepted'), ('REJ', 'Rejected'), ('PEN', 'Pending')], max_length=8, blank=True, default='PEN'),
        ),
    ]
