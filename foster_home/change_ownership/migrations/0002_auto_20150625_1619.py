# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('change_ownership', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='ownershiprequest',
            unique_together=set([('applicant', 'content_type', 'object_id')]),
        ),
    ]
