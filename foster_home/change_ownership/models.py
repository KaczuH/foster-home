from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.generic import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.db import models
from django.utils.translation import gettext_lazy as _

user_model = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


class OwnershipRequestManager(models.Manager):
    def place_request(self, applicant, obj):
        if getattr(obj, 'pk', None) is None:
            raise obj.DoesNotExist('Unsaved {} cannot be requested for ownership'.format(obj))
        request = self.create(applicant=applicant, content_object=obj)
        return request


class OwnershipRequest(models.Model):
    PENDING = 'PEN'
    REJECTED = 'REJ'
    ACCEPTED = 'ACC'
    DONE = 'DONE'
    REQUEST_STATUSES = (
        (ACCEPTED, _('Accepted')),
        (REJECTED, _('Rejected')),
        (PENDING, _('Pending')),
        (DONE, _('Done')),
    )

    status = models.CharField(max_length=8, choices=REQUEST_STATUSES, default=PENDING, blank=True)
    applicant = models.ForeignKey(user_model)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    applicant_confirmation = models.BooleanField(default=False)
    owner_confirmation = models.BooleanField(default=False)

    objects = OwnershipRequestManager()

    class Meta:
        unique_together = (('applicant', 'content_type', 'object_id'),)

    def __str__(self):
        return u'{} for ownership of {}'.format(self.applicant, self.content_object)

    def get_unaccepted(self):
        return self.content_object.ownership_requests.filter(status=self.PENDING)

    def set_applicant_confirmation(self, applicant, confirmation=True):
        if applicant != self.applicant:
            raise PermissionDenied('Only applicant can set confirmation of request')
        self.applicant_confirmation = confirmation

    def set_owner_confirmation(self, owner, confirmation=True):
        if owner != self.content_object.owner:
            raise PermissionDenied('Only owner can set confirmation of request')
        self.owner_confirmation = confirmation


class RequestedModelAbstract(models.Model):
    ownership_requests = GenericRelation(OwnershipRequest)

    class Meta:
        abstract = True

    def request_ownership(self, applicant=None):
        if not isinstance(applicant, user_model):
            raise TypeError("Only User can request ownership")

        OwnershipRequest.objects.place_request(applicant, self)

    def requested_by(self, applicant):
        if self.ownership_requests.filter(applicant_id=applicant.pk).exists():
            return True
        return False

    def during_transition(self):
        return self.ownership_requests.filter(status=OwnershipRequest.ACCEPTED).exists()