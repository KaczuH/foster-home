from django.contrib import admin

from .models import OwnershipRequest

admin.site.register(OwnershipRequest)
