from django.forms import ALL_FIELDS
from django.views.generic import CreateView
from django.views.generic import UpdateView

from .models import OwnershipRequest


class CreateOwnershipRequestView(CreateView):
    model = OwnershipRequest
    fields = ALL_FIELDS


class UpdateOwnershipRequestStatusView(UpdateView):
    model = OwnershipRequest
    fields = ('status',)