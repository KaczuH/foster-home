from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^$', 'users.views.landing_page_view', name='landing_page'),

    # TODO(kniski) find how to put login urlconf into users app
    url(r'^signin/$', 'users.views.signin_view', name='signin'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^signup/$', 'users.views.signup_view', name='signup'),
    url(r'^users/', include('users.urls', namespace='users')),  # TODO(kniski) design url for users list and user profile
    url(r'^animals/', include('animals.urls', namespace='animals')),

    # Examples:
    # url(r'^$', 'foster_home.views.home', name='home'),
    # url(r'^foster_home/', include('foster_home.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]

# Uncomment the next line to serve media files in dev.
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
