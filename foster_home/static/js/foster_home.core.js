var fstrhm = (function(window, document, undefined) {

    var reqOwnershipBtns = $('button#request-ownership');
    if (reqOwnershipBtns.length) {
        reqOwnershipBtns.click(function(e) {
            e.preventDefault();
            console.log('reqOwnBtn clicked');
            var btn = $(this);

            // prevent sending request for already requested animal
            if (btn.hasClass('active')) {
                return;
            }
            $.ajax({
                url: '/animals/request-ownership/',
                type: 'POST',
                cache: false,
                data: {
                    object_id: btn.data('animal'),
                    csrfmiddlewaretoken: btn.data('csrf')
                },
                dataType: 'json',
                beforeSend: function() {
                    btn.button('loading');
                },
                success: function(data, textStatus, XMLHttpRequest) {
                    btn.button('complete');
                    btn.addClass('active');
                }
            });
        });
    }

    var reqOwnershipAcc = $('.ownershipreq');
    if (reqOwnershipAcc.length) {
        reqOwnershipAcc.click(function(e) {
            e.preventDefault();
            console.log('reqOwnUpdt clicked');
            var btn = $(this),
                pk = btn.closest('li').data('ownership'),
                status = btn.data('status');

            $.ajax({
                url: '/animals/request-ownership-' + pk + '/',
                type: 'POST',
                cache: false,
                data: {
                    status: status,
                    csrfmiddlewaretoken: btn.closest('ul').data('csrf')
                },
                dataType: 'json',
                beforeSend: function() {
                    btn.button('loading');
                },
                success: function(data, textStatus, XMLHttpRequest) {
                    btn.button('complete');
                    btn.addClass('active');
                }
            });

        });
    }

    var reqOwnershipAcc = $('#ownershipreq-confirmation');
    if (reqOwnershipAcc.length) {
        reqOwnershipAcc.click(function(e) {
            e.preventDefault();
            console.log('reqOwnConfirmation clicked');
            var btn = $(this),
                animal_id = btn.data('id');

            $.ajax({
                url: '/animals/animal-' + animal_id + '/confirm-change//',
                type: 'GET',
                cache: false,
                dataType: 'json',
                beforeSend: function() {
                    btn.button('loading');
                },
                success: function(data, textStatus, XMLHttpRequest) {
                    btn.button('complete');
                    btn.addClass('active');
                }
            });

        });
    }

    var fstrhm = fstrhm || {};

    return fstrhm;
})(window, document);