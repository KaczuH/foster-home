from django.conf import settings
from django.contrib.contenttypes.generic import GenericRelation
from django.core.serializers import serialize
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import gettext_lazy as _
from django.core.urlresolvers import reverse

from users.models import BaseUser
from utils.model_utils import JSONField
from utils.model_utils import TimestampedModelMixin
from change_ownership.models import RequestedModelAbstract
from change_ownership.models import OwnershipRequest
from gallery.models import Image

# TODO (kniski)
# Animal fields we want to check differences on
DIFF_FIELDS = ('name', 'age', 'species', 'breed', 'owner', 'sex')


@python_2_unicode_compatible
class AnimalSpecies(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return u'{}'.format(self.name)


@python_2_unicode_compatible
class AnimalBreed(models.Model):
    name = models.CharField(max_length=128)
    species = models.ForeignKey(AnimalSpecies, related_name='breeds')

    def __str__(self):
        return u'{}'.format(self.name)


class BaseAnimal(models.Model):

    species = models.ForeignKey(AnimalSpecies, related_name='animals')
    breed = models.ForeignKey(AnimalBreed, related_name='animals')
    age = models.IntegerField()
    sex = models.CharField(max_length=2, choices=[('M', _('Male'),),
                                                  ('F', _('Female'),)])

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Animal(RequestedModelAbstract, TimestampedModelMixin, BaseAnimal):
    name = models.CharField(max_length=128)
    owner = models.ForeignKey(BaseUser, related_name='animals',
                              help_text=_('Current owner of given animal.'))
    photos = GenericRelation(Image)

    def __str__(self):
        return u'{} {} {}'.format(self.name, self.species, self.breed)

    def get_absolute_url(self):
        return reverse('animals:detail', kwargs={'pk': self.pk})

    def diff(self, old):
        """
        Returns dict containing field values that two Animals instances differs
        """
        if not isinstance(old, Animal):
            raise TypeError("{} is not instance of Animal".format(old))
        diff_fields = DIFF_FIELDS
        differences = {}
        for field_name in diff_fields:
            current_field = getattr(self, field_name, None)
            old_field = getattr(old, field_name, None)
            if current_field != old_field:
                if isinstance(current_field, models.Model) and isinstance(old_field, models.Model):
                    current_field = serialize('json', [current_field])
                    old_field = serialize('json', [old_field])
                differences[field_name] = {'current': current_field,
                                           'old': old_field}
        return differences

    def is_owner(self, user):
        return self.owner == user

    def is_future_owner(self, user):
        try:
            return self.ownership_requests.get(status=OwnershipRequest.ACCEPTED).applicant == user
        except OwnershipRequest.DoesNotExist:
            return False


class AnimalHistoryManager(models.Manager):
    use_in_migrations = True

    def log_action(self, owner_id, animal_id, diff):
        log_entry = self.model(owner_id=owner_id, animal_id=animal_id, diff=diff)
        log_entry.save()


@python_2_unicode_compatible
class AnimalHistory(models.Model):
    # TODO (kniski) add history entry category/flag
    entry_date = models.DateTimeField(_('entry date'), auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, help_text=_('Owner who added this entry'))
    animal = models.ForeignKey(Animal, blank=True, null=True, related_name='history')
    diff = JSONField(_('Animal version'))

    objects = AnimalHistoryManager()

    class Meta:
        verbose_name = _('animal history entry')
        verbose_name_plural = _('animal history entries')
        ordering = ('-entry_date',)

    def __repr__(self):
        return u'HistoryEntry: {} {}'.format(self.animal, self.entry_date)

    def changed(self):
        return self.animal.diff(self.diff)
