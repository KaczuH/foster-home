#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.animal_list_view, name='list'),
    url(r'^new-animal/$', views.animal_create_view, name='create'),
    url(r'^request-ownership/$', views.create_animal_ownership_request_view, name='create_ownership_request'),
    url(r'^request-ownership-(?P<pk>[0-9]+)/$', views.update_animal_ownership_request_status_view,
        name='update_ownership_request_status'),
    # url(r'^request-ownership/reject$', views.reject_animal_ownership_request_view,
    #     name='reject_ownership_request'),
    url(r'^animal-(?P<pk>[0-9]+)/$', views.animal_detail_view, name='detail'),
    url(r'^animal-(?P<pk>[0-9]+)/history/$', views.animalhistory_create_view, name='history'),
    url(r'^animal-(?P<pk>[0-9]+)/edit/$', views.animal_edit_view, name='edit'),
    url(r'animal-(?P<pk>[0-9]+)/confirm-change/$', views.set_ownership_confirmation_view,
        name='ownership_confirmation'),
]