#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.models import ContentType
from django.core.validators import EMPTY_VALUES

from .models import Animal
from .models import AnimalSpecies
from .models import AnimalBreed
from .models import AnimalHistory
from gallery.models import Image


class AnimalCreateForm(forms.ModelForm):
    photo_1 = forms.ImageField(required=False)
    photo_2 = forms.ImageField(required=False)

    class Meta:
        model = Animal
        fields = ['name', 'species', 'breed', 'age', 'sex', 'owner']

    def save_images(self):
        img_kwargs = {
            'content_type': ContentType.objects.get_for_model(self.instance),
            'object_id': self.instance.pk
        }
        image_1 = Image(image=self.files['photo_1'], **img_kwargs)
        image_2 = Image(image=self.files['photo_2'], **img_kwargs)
        image_1.save()
        image_2.save()


class AnimalModelFilterForm(forms.Form):
    species = forms.ModelChoiceField(label=_('Species'),
                                     required=False,
                                     queryset=AnimalSpecies.objects.all(),
                                     to_field_name='name')
    breed = forms.ModelChoiceField(label=_('Breed'),
                                   required=False,
                                   queryset=AnimalBreed.objects.all(),
                                   to_field_name='name')
    sex = forms.ChoiceField(label=_('Sex'),
                            required=False,
                            choices=[(None, _('All'),),
                                     ('M', _('Male'),),
                                     ('F', _('Female'),)])
    sort_by = forms.ChoiceField(label=_('Sorting order'),
                                choices=[('age', _('Od najmłodszego'),),
                                         ('-age', _('Od najstarszego'),)])

    def get_ordering(self):
        if not hasattr(self, 'ordering'):
            self.ordering = self.cleaned_data.pop('sort_by', None)
        return self.ordering

    def save(self):
        cd = self.cleaned_data
        self.get_ordering()
        filter_kwargs = {}
        for label in self.fields.keys():
            try:
                value = cd[label]
                if value in EMPTY_VALUES:
                    continue
                filter_kwargs[label] = value
            except KeyError:
                continue
        return filter_kwargs


class AnimalHistoryForm(forms.ModelForm):
    class Meta:
        model = AnimalHistory
        fields = ['owner', 'animal']
