from django.contrib import admin

from . import models


class AnimalSpeciesAdmin(admin.ModelAdmin):
    list_display = ('name',)


class AnimalBreedAdmin(admin.ModelAdmin):
    list_display = ('name', 'species', )


class AnimalHistoryInline(admin.TabularInline):
    model = models.AnimalHistory
    readonly_fields = ('entry_date', 'owner')


class AnimalAdmin(admin.ModelAdmin):
    list_display = ('name', 'age', 'sex', 'species', 'breed')
    inlines = [AnimalHistoryInline, ]


class AnimalHistoryAdmin(admin.ModelAdmin):
    list_display = ('entry_date', 'animal', 'owner', )


admin.site.register(models.AnimalSpecies, AnimalSpeciesAdmin)
admin.site.register(models.AnimalBreed, AnimalBreedAdmin)
admin.site.register(models.Animal, AnimalAdmin)
admin.site.register(models.AnimalHistory, AnimalHistoryAdmin)
