# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('animals', '0004_auto_20150413_1453'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='animalhistory',
            name='user',
        ),
        migrations.AddField(
            model_name='animalhistory',
            name='owner',
            field=models.ForeignKey(help_text='Owner who added this entry', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
