# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import animals.models
import utils.model_utils


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0005_auto_20150413_1503'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='animalhistory',
            managers=[
                ('objects', animals.models.AnimalHistoryManager()),
            ],
        ),
        migrations.RemoveField(
            model_name='animalhistory',
            name='description',
        ),
        migrations.AddField(
            model_name='animalhistory',
            name='diff',
            field=utils.model_utils.JSONField(verbose_name='Version of model', default='[{}]'),
            preserve_default=False,
        ),
    ]
