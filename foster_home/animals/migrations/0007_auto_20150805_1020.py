# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import utils.model_utils


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0006_auto_20150621_1613'),
    ]

    operations = [
        migrations.AlterField(
            model_name='animalhistory',
            name='diff',
            field=utils.model_utils.JSONField(verbose_name='Animal version'),
        ),
    ]
