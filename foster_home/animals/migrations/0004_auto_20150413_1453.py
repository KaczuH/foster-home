# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('animals', '0003_animal_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnimalHistory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('entry_date', models.DateTimeField(verbose_name='entry date', auto_now_add=True)),
                ('description', models.TextField(blank=True, verbose_name='description')),
            ],
            options={
                'verbose_name': 'animal history entry',
                'verbose_name_plural': 'animal history entries',
                'ordering': ('-entry_date',),
            },
        ),
        migrations.AlterField(
            model_name='animal',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, help_text='Current owner of given animal.', related_name='animals'),
        ),
        migrations.AddField(
            model_name='animalhistory',
            name='animal',
            field=models.ForeignKey(to='animals.Animal', blank=True, null=True, related_name='history'),
        ),
        migrations.AddField(
            model_name='animalhistory',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
