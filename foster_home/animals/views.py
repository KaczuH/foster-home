from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse_lazy
from django.forms.models import modelform_factory
from django.forms.widgets import HiddenInput
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.http import QueryDict
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import FormView
from django.views.generic import ListView
from django.views.generic import UpdateView
from django.views.generic.edit import FormMixin
from django.views.generic.detail import SingleObjectMixin

from .forms import AnimalCreateForm
from .forms import AnimalHistoryForm
from .forms import AnimalModelFilterForm
from .models import Animal
from .models import AnimalHistory
from change_ownership.models import OwnershipRequest
from change_ownership.views import CreateOwnershipRequestView
from change_ownership.views import UpdateOwnershipRequestStatusView

ANIMAL_CONTENTYPE_ID = ContentType.objects.get(app_label="animals", model="animal").pk


class AnimalCreateView(CreateView):
    template_name_suffix = '_add'
    model = Animal
    form_class = AnimalCreateForm
    fields = ('name', 'species', 'breed', 'age', 'sex', 'owner')

    def get_form_class(self):
        widgets = {'owner': HiddenInput}
        return modelform_factory(self.model, form=AnimalCreateForm, fields=self.fields, widgets=widgets)

    def get_initial(self):
        initial = super().get_initial()
        initial['owner'] = self.request.user.pk
        return initial

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.FILES:
            form.save_images()
        return response


class AnimalDetailView(DetailView):
    model = Animal

    def get_context_data(self, **kwargs):
        user = self.request.user
        context = super().get_context_data(**kwargs)
        context['already_requested'] = self.object.requested_by(user)
        context['is_owner'] = self.object.is_owner(user)
        context['is_future_owner'] = self.object.is_future_owner(user)
        return context


class AnimalEditView(UpdateView):
    model = Animal
    fields = ('name', 'age', 'species', 'breed')

    def form_valid(self, form):
        """
        Saves changes made to Animal and logs previous version on animal model.
        """
        old_object = self.get_object()
        self.object = form.save()
        current_object = self.object
        assert old_object.pk == current_object.pk
        diff = current_object.diff(old_object)
        AnimalHistory.objects.log_action(self.request.user.pk,
                                         current_object.pk,
                                         diff)
        return HttpResponseRedirect(self.get_success_url())


# TODO(kniski) add permission checking, only animal owner can add history entry
class AnimalHistoryCreateView(SingleObjectMixin, FormView):
    form_class = AnimalHistoryForm
    model = Animal
    template_name = 'animals/animalhistory_form.html'

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().dispatch(request, args, kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = {'description': self.request.POST['description'],
                    'animal': self.object.pk,
                    'owner': self.object.owner.pk}
            kwargs['data'] = data
        return kwargs

    def get_success_url(self):
        return reverse_lazy('animals:detail', kwargs=self.kwargs)

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


# FIXME (kniski) it feels this view could be simpler
class AnimalListView(FormMixin, ListView):
    model = Animal
    form_class = AnimalModelFilterForm

    def get_queryset(self):
        queryset = self.model._default_manager.all()
        if self.queryset is not None:
            queryset = self.queryset
        ordering = self.get_ordering()
        if ordering:
            queryset = queryset.order_by(ordering)
        return queryset

    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        response.context_data['form'] = self.get_form()
        return response

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            self.ordering = form.get_ordering()
            self.queryset = self.model._default_manager.filter(**form.save())
            return self.get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse_lazy('animals:list'))


class CreateAnimalOwnershipRequestView(CreateOwnershipRequestView):
    http_method_names = ['post']

    def notify_owner(self):
        owner = self.object.content_object.owner
        applicant = self.object.applicant
        subject = _('New adoption application')
        message = """
We are happy to inform you that someone wishes to adopt your animal.
You can verify profile of candidate by clicking link below.
{}""".format(self.request.build_absolute_uri(applicant.get_absolute_url()))
        owner.email_user(subject, message)

    def form_valid(self, form):
        self.object = form.save()
        self.notify_owner()
        return JsonResponse({'success': True})

    def form_invalid(self, form):
        return JsonResponse({'success': False, 'errors': form.errors})

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        new_data = QueryDict('', mutable=True)
        new_data.update({'applicant': self.request.user.pk, 'content_type': ANIMAL_CONTENTYPE_ID})
        new_data.update(kwargs['data'])
        kwargs['data'] = new_data
        return kwargs


class UpdateAnimalOwnershipRequestStatusView(UpdateOwnershipRequestStatusView):
        http_method_names = ['post']
        success_url = reverse_lazy('animals:list')

        def send_reject_notification(self, user):
            subject = 'Ownership request rejected'
            message = """
We are sorry to inform you that your request to adopt {} has been rejected.
            """.format(self.object.content_object.name)
            user.email_user(subject, message)

        def send_accepted_notification(self):
            subject = 'Ownership request accepted'
            message = """
We are happy to inform you that your request to adopt {} has been accepted.
            """.format(self.object.content_object.name)
            self.object.applicant.email_user(subject, message)

        def form_valid(self, form):
            self.object = object = form.save()

            if object.status == object.REJECTED:
                self.send_reject_notification(object.applicant)
                object.delete()
            else:
                unaccepted_reqs = object.get_unaccepted()
                for unaccepted in unaccepted_reqs:
                    self.send_reject_notification(unaccepted.applicant)
                unaccepted_reqs.delete()
                self.send_accepted_notification()
            return JsonResponse({'success': True})

        def form_invalid(self, form):
            return JsonResponse({'success': False, 'errors': form.errors})


def SetOwnershipConfirmationView(request, pk):
    user = request.user
    animal = Animal.objects.get(pk=int(pk))
    ownership_req = animal.ownership_requests.get(status=OwnershipRequest.ACCEPTED)
    if animal.is_owner(user):
        ownership_req.set_owner_confirmation(user)
        ownership_req.status = OwnershipRequest.DONE
        ownership_req.save()
    elif animal.is_future_owner(user):
        ownership_req.set_applicant_confirmation(user)
        ownership_req.status = OwnershipRequest.DONE
        ownership_req.save()
    else:  # Neither owner or applicant
        return JsonResponse({'succes': False})

    if ownership_req.applicant_confirmation and ownership_req.owner_confirmation:
        animal.owner = ownership_req.applicant
        animal.save()

    return JsonResponse({'success': True})


animal_create_view = AnimalCreateView.as_view()
animal_detail_view = AnimalDetailView.as_view()
animal_list_view = AnimalListView.as_view()
animal_edit_view = AnimalEditView.as_view()
animalhistory_create_view = AnimalHistoryCreateView.as_view()
create_animal_ownership_request_view = CreateAnimalOwnershipRequestView.as_view()
update_animal_ownership_request_status_view = UpdateAnimalOwnershipRequestStatusView.as_view()
set_ownership_confirmation_view = SetOwnershipConfirmationView