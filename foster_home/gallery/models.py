import os
import time

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.generic import GenericForeignKey
from django.db import models


def upload_image(instance, filename):
    base_name, extension = os.path.splitext(filename)
    new_filename = '{}_{}_{}{}'.format(instance.content_object.name, instance.content_object.owner,
                                       int(time.time()), extension)
    model_name = instance.content_object._meta.model_name
    app_label = instance.content_object._meta.app_label
    directory = os.path.join(app_label, model_name)
    if not os.path.exists(directory):
        os.makedirs(directory)

    return os.path.join(directory, new_filename)


class AbstractImage(models.Model):
    image = models.ImageField(upload_to=upload_image)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:
        abstract = True


class Image(AbstractImage):

    class Meta:
        verbose_name = 'image'
