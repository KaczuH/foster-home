#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.forms import AuthenticationForm
from django.template import Library

register = Library()

@register.inclusion_tag('signin_form_fields.html')
def signin_form_fields():
    return {'form': AuthenticationForm()}
