#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<username>[0-9A-Za-z_\-]+)/$', views.baseuser_detail_view, name='user_profile'),
    url(r'^(?P<username>[0-9A-Za-z_\-]+)/edit$', views.baseuser_edit_view, name='edit'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.user_activation_view, name='activation'),
]
