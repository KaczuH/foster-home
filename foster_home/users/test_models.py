from django.core.exceptions import ValidationError
from django.test import TestCase

from .models import BaseUser

class BaseUserTest(TestCase):

    def setUp(self):
        BaseUser.objects.create_user(username='testusr',
                                     email='test@example.com',
                                     password='testpsswd')
    def test_new_user_is_inactive(self):
        user_inactive = BaseUser.objects.get(username='testusr')
        self.assertFalse(user_inactive.is_active)

    def test_get_activation_key(self):
        user = BaseUser.objects.get(username='testusr')
        key_template = '{username}.{email}.{date_joined}.{pk}'
        kwargs = {'username': 'testusr',
                  'email': 'test@example.com',
                  'date_joined': int(user.date_joined.timestamp()),
                  'pk': user.pk}
        key = key_template.format(**kwargs)
        self.assertEqual(user.get_activation_key(), key)

    def test_get_activation_salt(self):
        user = BaseUser.objects.get(username='testusr')
        salt = '{}.{}.activation_link'.format(user.pk, user.username)
        self.assertEqual(user.get_activation_salt(), salt)

    def test_verify_activation_signature(self):
        user = BaseUser.objects.get(username='testusr')
        self.assertFalse(user.verify_activation_signature('bad_signature'))

        signature = user.generate_activation_uid()
        self.assertTrue(user.verify_activation_signature(signature))
        expired_signature = '1YfGxJ:8WpsGfQGm7fxUtJ7b5coun-j54Q'
        self.assertFalse(user.verify_activation_signature(expired_signature, max_age=10))

        user.is_active = True
        self.assertRaises(ValidationError, user.verify_activation_signature, 'empty_signature')
