from django.test import TestCase, override_settings

from .forms import SignUpForm
from .models import BaseUser


@override_settings(PASSWORD_HASHERS=['django.contrib.auth.hashers.SHA1PasswordHasher'])
class SignUpFormTest(TestCase):

    data = {'username': 'jim',
            'password1': 'password',
            'password2': 'password',
            'email': 'jim@example.com',
            'terms_and_conds': True}

    def test_email_field_required(self):
        data = self.data
        form = SignUpForm(data)
        self.assertTrue(form.is_valid())

        del data['email']
        form = SignUpForm(data)
        self.assertFalse(form.is_valid())

    def test_form_save(self):
        form = SignUpForm(self.data)
        form.is_valid()
        form.register_user(commit=False)

        self.assertFalse(BaseUser.objects.filter(username='jim').exists())
        form = SignUpForm(self.data)
        form.is_valid()
        form.register_user(commit=True)
        self.assertEqual(repr(BaseUser.objects.get(username='jim')), '<BaseUser: jim>')
