import logging

from django.conf import settings
from django.core import signing
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.core.urlresolvers import reverse
from django.contrib.auth import models as auth_models
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _

log = logging.getLogger(__name__)


class UserManager(auth_models.BaseUserManager):

    def _create_user(self, username, email, password, is_active,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True, True,
                                 **extra_fields)


class BaseUser(auth_models.AbstractUser):
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message=_("""Phone number must be in entered
                                 in the format: '+999999999'. Up to 15 digits
                                 allowed."""))

    phone_no = models.CharField(max_length=15, blank=True, validators=[phone_regex])
    address = models.CharField(max_length=100, blank=True)
    call_before = models.TimeField(null=True, blank=True,
                                   help_text=_("""User can specify time beyond
                                    which he wouldn't pick up phone."""))

    objects = UserManager()

    def get_activation_key(self):
        key_template = '{username}.{email}.{date_joined}.{pk}'
        kwargs = {'username': self.username,
                  'email': self.email,
                  'date_joined': int(self.date_joined.timestamp()),
                  'pk': self.pk}
        return key_template.format(**kwargs)

    def get_activation_salt(self):
        return '{}.{}.activation_link'.format(self.pk, self.username)

    def generate_activation_uid(self):
        if self.is_active:
            return

        key = self.get_activation_key()
        salt = self.get_activation_salt()
        signer = signing.TimestampSigner(salt=salt)
        # (kniski) 'value' is redundant
        value, signature = signer.sign(key).split(':', 1)
        return signature

    def verify_activation_signature(self, signature,
                                    max_age=settings.ACTIVATION_LINK_MAX_AGE):

        if self.is_active:
            raise ValidationError(_('User is already active.'))

        signer = signing.TimestampSigner(salt=self.get_activation_salt())
        signature = '{}:{}'.format(self.get_activation_key(), signature)
        try:
            signer.unsign(signature, max_age=max_age)
        except signing.SignatureExpired:
            log.warning(_("User: %s signature expired."), self)
            return False
        except signing.BadSignature:
            log.warning(_("User: %s, bad signature."), self)
            return False
        return True

    def get_absolute_url(self):
        return reverse('users:user_profile', kwargs={'username': self.username})
