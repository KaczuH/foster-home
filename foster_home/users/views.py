import logging

from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import FormView
from django.views.generic import UpdateView
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode

from .forms import SignUpForm
from .models import BaseUser
from utils.views_utils import LoginView

log = logging.getLogger('foster_home.{}'.format(__name__))


class LandingPageView(TemplateView):
    template_name = 'landing_page.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['signup_form'] = SignUpForm()
        return context


class SignInView(LoginView):
    template_name = 'signin.html'

    def get_success_url(self):
        self.success_url = reverse('users:user_profile',
                                   kwargs={'username': self.request.user.username})
        return super().get_success_url()


class SignUpView(FormView):
    form_class = SignUpForm
    template_name = 'signup.html'
    success_url = reverse_lazy('landing_page')

    def form_valid(self, form):
        user = form.register_user()
        ctx = {
            'protocol': self.request.scheme,
            'domain': self.request.get_host()
        }
        msg_body = 'Your activation key: {}'.format(user.generate_activation_uid())
        send_mail('REGISTRATION', msg_body, 'registration@foster_home.org', [user.email])
        return super().form_valid(form)


class UserActivationView(TemplateView):
    template_name = 'activation_failure.html'

    def get_user(self):
        user_model = get_user_model()
        user_pk = force_text(urlsafe_base64_decode(self.kwargs['uid64']))
        try:
            user = user_model.objects.get(pk=user_pk, is_active=False)
        except user_model.DoesNotExist:
            user = None
        return user

    def signature_valid(self, user):
        user.is_active = True
        user.save()
        return HttpResponseRedirect(reverse_lazy('users:signature_valid'))

    def signature_invalid(self):
        return self.render_to_response(self.get_context_data())

    def get(self, request, *args, **kwargs):
        # response = super().get(request, *args, **kwargs)
        user = self.get_user()
        signature = self.kwargs['signature']
        if user is not None and user.valid_signature(signature):
            return self.signature_valid(user)
        else:
            return self.signature_invalid()


class BaseUserDetailView(DetailView):
    model = BaseUser
    slug_url_kwarg = 'username'
    slug_field = 'username'


class BaseUserEditView(UpdateView):
    model = BaseUser
    fields = ('phone_no', 'address', 'call_before')
    slug_field = 'username'
    slug_url_kwarg = 'username'


landing_page_view = LandingPageView.as_view()
signin_view = SignInView.as_view()
signup_view = SignUpView.as_view()
user_activation_view = UserActivationView.as_view()
baseuser_detail_view = BaseUserDetailView.as_view()
baseuser_edit_view = BaseUserEditView.as_view()
