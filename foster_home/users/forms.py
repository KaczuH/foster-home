#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import forms as auth_forms
from django.utils.translation import ugettext as _

from .models import BaseUser


class SignUpForm(auth_forms.UserCreationForm):

    # FIXME(kniski) it should be enforced on model level
    email = forms.EmailField(label=_('Email'), required=True)
    terms_and_conds = forms.BooleanField()

    class Meta(auth_forms.UserCreationForm.Meta):
        model = BaseUser
        fields = ("username",)

    def clean_username(self):
        # Since User.username is unique, this check is redundant,
        # but it sets a nicer error message than the ORM. See #13147.
        username = self.cleaned_data["username"]
        try:
            self._meta.model._default_manager.get(username=username)
        except BaseUser.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',
        )

    def register_user(self, commit=True):
        cleaned_data = self.cleaned_data
        del cleaned_data['terms_and_conds']
        cleaned_data['password'] = cleaned_data['password1']
        del cleaned_data['password1']
        del cleaned_data['password2']

        if commit:
            user = BaseUser.objects.create_user(**cleaned_data)
        else:
            user = BaseUser(**cleaned_data)
        return user