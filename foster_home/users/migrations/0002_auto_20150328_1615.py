# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseuser',
            name='call_before',
            field=models.TimeField(help_text="User can specify time beyond\n                                    he wouldn't pick up phone.", null=True),
            preserve_default=True,
        ),
    ]
