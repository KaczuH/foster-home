# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20150328_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='baseuser',
            name='address',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='call_before',
            field=models.TimeField(blank=True, null=True, help_text="User can specify time beyond\n                                    which he wouldn't pick up phone."),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='email',
            field=models.EmailField(blank=True, verbose_name='email address', max_length=254),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='groups',
            field=models.ManyToManyField(help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', blank=True, to='auth.Group', related_query_name='user', verbose_name='groups', related_name='user_set'),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='last_login',
            field=models.DateTimeField(blank=True, null=True, verbose_name='last login'),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='phone_no',
            field=models.CharField(blank=True, validators=[django.core.validators.RegexValidator(message="Phone number must be in entered\n                                 in the format: '+999999999'. Up to 15 digits\n                                 allowed.", regex='^\\+?1?\\d{9,15}$')], max_length=15),
        ),
        migrations.AlterField(
            model_name='baseuser',
            name='username',
            field=models.CharField(error_messages={'unique': 'A user with that username already exists.'}, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters.', 'invalid')], max_length=30, help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True),
        ),
    ]
