from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext as _

from .models import BaseUser
from animals.models import Animal


class AnimalInline(admin.TabularInline):
    model = Animal
    readonly_fields = ('name', 'species', 'breed', 'age', 'sex',)
    extra = 0


class BaseUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = BaseUser

class BaseUserAdmin(UserAdmin):
    form = BaseUserChangeForm
    inlines = [AnimalInline,]

    # FIXME(kniski) This is shamelessly copied from django.contrib.auth.admin I didn't find
    # better solution
    fieldsets = (
    (None, {'fields': ('username', 'password')}),
    (_('Personal info'), {'fields': ('first_name', 'last_name', 'email',
                                     'phone_no', 'address', 'call_before')}),
    (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                   'groups', 'user_permissions')}),
    (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
)
admin.site.register(BaseUser, BaseUserAdmin)