Install
=========

All following instructions are tested on Ubuntu 14.04 LTS

Saltstack Setup
--------------

We are going to bootstrap masterless minion

Once you established ssh connection run commands below:
    curl -L https://bootstrap.saltstack.com -o install_salt.sh
    sudo sh install_salt.sh -D -X -U -P git stable

On Ubuntu minion deamon could be running despite passing an -X option to
bootstrap script. Disable it:
    sudo service salt-minion status
    echo manual | sudo tee /etc/init/salt-minion.override

For now our configuration is working with masterless minion. There are two ways
to run salt minion masterless. First: change 'file_client' parameter to local in minion
configuration file. Second: Invoke salt-call command passing --local argument.
Choose what is more convinient for you.

Due to initial lack of knowledge of Saltstack features pillar root settings are
not default. Change it to:

    pillar_roots:
        base:
            - /srv/salt/pillar


Bitbucket Deployment Keys
-------------------------

If not present generate ssh keys by running:

    ssh-keygen -t rsa -b 4096 -C "$(whoami)@$(hostname)-$(date -I)"
    Generating public/private rsa key pair.
    Enter file in which to save the key (/root/.ssh/id_rsa): /root/.ssh/foster_home_id_rsa
    Enter passphrase (empty for no passphrase): (Provide if you want)
    Enter same passphrase again:
    Your identification has been saved in /root/.ssh/foster_home_id_rsa.
    Your public key has been saved in /root/.ssh/foster_home_id_rsa.pub.

Add generated key to bitbucket deployment keys on your repository management dashboard.


Execute to add bitbucket.org to known_hosts:
    ssh-keyscan -H -t rsa bitbucket.org >> /root/.ssh/known_hosts




Postgres Setup
--------------

First, switch to postgres user:
    sudo su -- postgres
Command prompt should now look like "postgres@yourserver". If it is the case
run command to create database.
    createdb foster_home
Now is time to create database user
    createuser -P
First three prompts are for database name and password, last three you should
type n as it is shown below.
    Shall the new role be a superuser? (y/n) n
    Shall the new role be allowed to create databases? (y/n) n
    Shall the new role be allowed to create more new roles? (y/n) n
Now to grant new user access to database type the following:
    psql
    GRANT ALL PRIVILEGES ON DATABASE foster_home TO foster_home;
You now have a PostgreSQL database and user to access that database with.


Django Setup

TODO: Add tutorial on Djanggo cloning repository etc


