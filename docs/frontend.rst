Frontend dev workflow
=====================
Styles, js scripts, imgs etc are placed in ``static`` directory. Please follow
the structure of files.

Before deployment sequence of ``gulp`` tasks are run to compile, minify, optimize and concatenate
all necessary files that are served as static by nginx.

To run this task:
::
    gulp dist

Css
---

Project uses SASS for writing styles. Any overrides and custom styles goes to
``_foster_home_overrides.scss``. All vendor components should be imported in main
SASS file ``foster_home.scss``

Any vendor styles should be installed via bower by adding needed packages to
``bower.json``.


Javascript
----------
Any new JS scripts added by developer should be declared in ``gulpfile`` in order to be processed by gulp
during deployment.